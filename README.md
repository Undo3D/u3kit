# [U3Kit](https://undo3d.gitlab.io/u3kit/)

#### Undo3D’s functional toolkit

__Coming Soon__  
Expect the first public beta mid-2019, and the first production release mid-2020.

[Docs](https://undo3d.gitlab.io/u3kit/docs/)  
[Examples](https://undo3d.gitlab.io/u3kit/examples/)  
[Tests](https://undo3d.gitlab.io/u3kit/tests/)  
[@Undo3D](https://twitter.com/Undo3D)  
[undo3d.com](https://undo3d.com/)  
[Repo](https://gitlab.com/Undo3D/u3kit)  
