/* U3Kit Ibeji 0.0.3 */
/**
 * A fast function for creating frozen clones from simple values.
 *
 * The value must be directly representable as a JSON file, so it can be:
 *   - A scalar, like "Hello Ibeji", 123.45, true or null
 *   - Made up of ‘plain’ objects or arrays, like { o:{} } or [ [], [] ]
 *   - A mixture of the above, like { a:[ 1, true, o:{ n:null } ], EMPTY:"" }
 *
 * The value must not contain:
 *   - Non-JSON types, like regexps, dates, sets, functions or undefined
 *   - Setters and getters @TODO describe better here, and test
 *   - Circular references - this will cause an infinite loop!
 *
 * Any part of the input value can be non-writable or non-configurable, or
 * Object.freeze()’d. The output value is completely deep frozen.
 *
 * The prototype chain is ignored, so your array’s custom `last()` method will
 * be discarded. Likewise, ‘non enumerable properties’ are ignored.
 *
 * If you don’t know whether your value is valid, use `isJsonable()`. @TODO
 *
 * `jsonable :: a -> a`
 * @param {*} value - a scalar value, plain object, or plain array
 * @return {*} - an immutable (deep frozen) copy
 *
 * @example
 *     import jsonable from './u3kit/src/ibeji/jsonable.js'
 *     import push from './u3kit/src/ibeji/push.js'
 *
 *     const fourEvens = jsonable([0,2,4,6])
 *     fourEvens[3] //=> 6
 *     fourEvens[4] = 8 // throws an exception
 *     fourEvens.push(8) // throws an exception
 *     fourEvens[4] //=> undefined
 *
 *     const fiveEvens = push(8, '', fourEvens)
 *     fiveEvens[4] //=> 8
 *
 * @example
 *     import { jsonable } from './u3kit/bundles/u3kit.esm.js'
 *
 * @public
 * @module u3kit/ibeji/jsonable
 */

const jsonable = value =>
    null === value || 'object' !== typeof value
      ? value
      : Object.freeze(
            Array.isArray(value)
              ? value.reduce(
                    (acc, element) =>
                        acc.concat([
                            null !== element
                            && 'object' === typeof element
                               ? jsonable(element)
                               : element,
                        ]), //concat()
                    []
                ) //reduce() an array
              : Object.getOwnPropertyNames(value).reduce(
                    (acc, key) =>
                        Object.assign(acc, { [key]:
                            value.hasOwnProperty(key)
                            && null !== value[key]
                            && 'object' === typeof value[key]
                                ? jsonable(value[key])
                                : value[key],
                        }), //assign()
                    {}
                ) //reduce() an object
        ); //freeze()


/*
// Nested objects.
const orig_1 = { a:1, b:2, c:{ d:1 } }
const frozen_1 = jsonable(orig_1)

console.log('Nested objects:')

if ( JSON.stringify(orig_1) !== JSON.stringify(frozen_1) )
    console.warn(`\x1b[31m✘ Is not deeply equal:\x1b[0m\n  ${JSON.stringify(orig_1)}\n  ${JSON.stringify(frozen_1)}`)
else
    console.log('\x1b[32m✓ Is deeply equal\x1b[0m')

if (orig_1 === frozen_1)
    console.warn('\x1b[31m✘ Is the original!\x1b[0m')
else
    console.log('\x1b[32m✓ Is a copy\x1b[0m')

try {
    frozen_1.a = 22
    console.error('\x1b[31m✘ Can edit top-level properties!\x1b[0m')
} catch (e) {
    console.log('\x1b[32m✓ Cannot edit top-level properties\x1b[0m')
}

try {
    frozen_1.c.d = 22
    console.error('\x1b[31m✘ Can edit 2nd-level properties!\x1b[0m')
} catch (e) {
    console.log('\x1b[32m✓ Cannot edit 2nd-level properties\x1b[0m')
}


// Nested arrays.
const orig_2 = [ 1, 2, [ 3, [ 4, 5 ], 6 ] ]
const frozen_2 = jsonable(orig_2)

console.log('Nested arrays:')

if ( JSON.stringify(orig_2) !== JSON.stringify(frozen_2) )
    console.warn(`\x1b[31m✘ Is not deeply equal:\x1b[0m\n  ${JSON.stringify(orig_2)}\n  ${JSON.stringify(frozen_2)}`)
else
    console.log('\x1b[32m✓ Is deeply equal\x1b[0m')


if (orig_2 === frozen_2)
    console.warn('\x1b[31m✘ Is the original!\x1b[0m')
else
    console.log('\x1b[32m✓ Is a copy\x1b[0m')

try {
    frozen_2[1] = 22
    console.error('\x1b[31m✘ Can edit top-level properties!\x1b[0m')
} catch (e) {
    console.log('\x1b[32m✓ Cannot edit top-level properties\x1b[0m')
}

try {
    frozen_2[2][1].push(22)
    console.error('\x1b[31m✘ Can edit 2nd-level properties!\x1b[0m')
} catch (e) {
    console.log('\x1b[32m✓ Cannot edit 2nd-level properties\x1b[0m')
}


// Mixture of objects and arrays.
const orig_3 = { a:1, b:[ 2, { c:3 } ], d:4 }
const frozen_3 = jsonable(orig_3)

console.log('Mixture of objects and arrays:')

if ( JSON.stringify(orig_3) !== JSON.stringify(frozen_3) )
    console.warn(`\x1b[31m✘ Is not deeply equal:\x1b[0m\n  ${JSON.stringify(orig_3)}\n  ${JSON.stringify(frozen_3)}`)
else
    console.log('\x1b[32m✓ Is deeply equal\x1b[0m')


if (orig_3 === frozen_3)
    console.warn('\x1b[31m✘ Is the original!\x1b[0m')
else
    console.log('\x1b[32m✓ Is a copy\x1b[0m')

try {
    frozen_3.b = 22
    console.error('\x1b[31m✘ Can edit top-level properties!\x1b[0m')
} catch (e) {
    console.log('\x1b[32m✓ Cannot edit top-level properties\x1b[0m')
}

try {
    delete frozen_3.b[1].c
    console.error('\x1b[31m✘ Can edit 2nd-level properties!\x1b[0m')
} catch (e) {
    console.log('\x1b[32m✓ Cannot edit 2nd-level properties\x1b[0m')
}
*/

/**
 * Xx.
 *
 * `push :: (...a, String, Object) -> Object`
 *
 * @param {...*} element - One or more values to push onto the end of the array
 * @param {String=} path - Specifies where the array is. Use "" for top level
 * @param {Object} subject - A plain object or array
 * @return {Object} - an immutable copy of the object or array
 *
 * @example
 *     import push from './u3kit/src/ibeji/push.js'
 *     @TODO
 *
 * @example
 *     import { push } from './u3kit/bundles/u3kit.esm.js'
 *     @TODO
 *
 * @public
 * @module u3kit/ibeji/push
 */

const push = (element, path, subject) => {
    if (! path)
        return jsonable( subject.concat([ element ]) )
    return { a:{ b:[ { oh:'ok' }, [1,3,5,7,9] ] } }
};

// import jsonable from './jsonable.js'

/**
 * Xx.
 *
 * `replace :: (...a, String, Object) -> Object`
 *
 * @param {*} value - The new value, to be insert where the old value was
 * @param {String=} path - Specifies the old value to be replaced
 * @param {Object} subject - A plain object or array
 * @return {Object} - an immutable copy of the object or array
 *
 * @example
 *     import replace from './u3kit/src/ibeji/replace.js'
 *     @TODO
 *
 * @example
 *     import { replace } from './u3kit/bundles/u3kit.esm.js'
 *     @TODO
 *
 * @public
 * @module u3kit/ibeji/replace
 */

const replace = (value, path, subject) => {
// path = 'fddf.ds[098]'
    path = path.split(/[.[\]]/).filter(p=>p); //@TODO deal with null match
    return Array.isArray(subject)
        ? _recursiveReplaceElement(value, path, subject)
        : _recursiveReplaceProperty(value, path, subject)
};


function _recursiveReplaceElement (value, pathArray, subjectArray) {
    const index = +pathArray[0];
    const replacement = 1 === pathArray.length
        ? value
        : Array.isArray(subjectArray[index])
            ? _recursiveReplaceElement(value, pathArray.slice(1), subjectArray[index])
            : _recursiveReplaceProperty(value, pathArray.slice(1), subjectArray[index]);
    const before = subjectArray.slice(0, index);
    const after = subjectArray.slice(index + 1);
    return [
        ...before,
        replacement,
        ...after,
    ]
}

function _recursiveReplaceProperty (property, pathArray, subjectObject) {
}

export { jsonable, push, replace };
