/* U3Kit Kbit 0.0.3 */
/**
 * Creates a function that always returns a given value.
 *
 * `always :: a -> (void -> a)`
 * @param {*} val - the value which will always be returned
 * @return {Function} takes no arguments, and only ever returns `val`
 *
 * @example
 *     import always from './u3kit/src/kbit/always.js'
 *     const T = always(true)
 *     T() //=> true
 *     T() //=> true
 *
 * @example
 *     import { always } from './u3kit/bundles/u3kit.esm.js'
 *     const fortyTwo = always(42)
 *     fortyTwo() //=> 42
 *     fortyTwo() //=> 42
 *
 * @public
 * @module u3kit/kbit/always
 */

const always = val => () => val;

/**
 * Same as `a && b`.
 *
 * `and :: (a, b) -> Boolean`
 * @param {*} a - @TODO
 * @param {*} b - @TODO
 * @return {Boolean} @TODO
 *
 * @example
 *     import and from './u3kit/src/kbit/and.js'
 *     and(true, true) // => true
 *
 * @example
 *     import { and } from './u3kit/bundles/u3kit.esm.js'
 *     and(false, true) // => false
 *
 * @public
 * @module u3kit/kbit/and
 */

const and = (a, b) => !! a && !! b;

/**
 * Same as `'function' === typeof value`.
 *
 * `* -> Boolean`
 * @param {*} val - the value to test
 * @return {Boolean} `true` if `val` is a function, `false` otherwise
 *
 * @example
 *     import isFunction from './u3kit/src/kbit/is-function.js'
 *     isFunction(x => x) // => true
 *
 * @example
 *     import { isFunction } from './u3kit/bundles/u3kit.esm.js'
 *     isFunction(function named () {}) // => true
 *     isFunction('any other value')    // => false
 *
 * @public
 * @module u3kit/kbit/is-function
 */

const isFunction = val => 'function' === typeof val;

// Flyd does it this way:
//     return !! ( val && val.constructor && val.call val obj.apply )

export { always, and, isFunction };
