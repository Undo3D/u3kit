/* U3Kit Ombla 0.0.3 */
/**
 * Same as `'function' === typeof value`.
 *
 * `* -> Boolean`
 * @param {*} val - the value to test
 * @return {Boolean} `true` if `val` is a function, `false` otherwise
 *
 * @example
 *     import isFunction from './u3kit/src/kbit/is-function.js'
 *     isFunction(x => x) // => true
 *
 * @example
 *     import { isFunction } from './u3kit/bundles/u3kit.esm.js'
 *     isFunction(function named () {}) // => true
 *     isFunction('any other value')    // => false
 *
 * @public
 * @module u3kit/kbit/is-function
 */

const isFunction = val => 'function' === typeof val;

// Flyd does it this way:
//     return !! ( val && val.constructor && val.call val obj.apply )

/**
 * Returns `true` if the supplied argument is a stream and `false` otherwise.
 *
 * `* -> Boolean`
 * @param {*} val - the value to test
 * @return {Boolean} `true` if `val` is an Ombla stream, `false` otherwise
 *
 * @example
 *     import isStream from './u3kit/src/ombla/is-stream.js'
 *     const s = stream(1)
 *     isStream(s) // => true
 *
 * @example
 *     import { isStream } from './u3kit/bundles/u3kit.esm.js'
 *     isStream(1) // => false
 *
 * @public
 * @module u3kit/ombla/is-stream
 */

const isStream = val => isFunction(val) && 'boolean' === typeof val.hasVal;

export { isStream };
