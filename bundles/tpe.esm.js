/* U3Kit Tpe 0.0.3 */
/**
 * A fast function for creating frozen clones from simple values.
 *
 * The value must be directly representable as a JSON file, so it can be:
 *   - A scalar, like "Hello Ibeji", 123.45, true or null
 *   - Made up of ‘plain’ objects or arrays, like { o:{} } or [ [], [] ]
 *   - A mixture of the above, like { a:[ 1, true, o:{ n:null } ], EMPTY:"" }
 *
 * The value must not contain:
 *   - Non-JSON types, like regexps, dates, sets, functions or undefined
 *   - Setters and getters @TODO describe better here, and test
 *   - Circular references - this will cause an infinite loop!
 *
 * Any part of the input value can be non-writable or non-configurable, or
 * Object.freeze()’d. The output value is completely deep frozen.
 *
 * The prototype chain is ignored, so your array’s custom `last()` method will
 * be discarded. Likewise, ‘non enumerable properties’ are ignored.
 *
 * If you don’t know whether your value is valid, use `isJsonable()`. @TODO
 *
 * `jsonable :: a -> a`
 * @param {*} value - a scalar value, plain object, or plain array
 * @return {*} - an immutable (deep frozen) copy
 *
 * @example
 *     import jsonable from './u3kit/src/ibeji/jsonable.js'
 *     import push from './u3kit/src/ibeji/push.js'
 *
 *     const fourEvens = jsonable([0,2,4,6])
 *     fourEvens[3] //=> 6
 *     fourEvens[4] = 8 // throws an exception
 *     fourEvens.push(8) // throws an exception
 *     fourEvens[4] //=> undefined
 *
 *     const fiveEvens = push(8, '', fourEvens)
 *     fiveEvens[4] //=> 8
 *
 * @example
 *     import { jsonable } from './u3kit/bundles/u3kit.esm.js'
 *
 * @public
 * @module u3kit/ibeji/jsonable
 */

const jsonable = value =>
    null === value || 'object' !== typeof value
      ? value
      : Object.freeze(
            Array.isArray(value)
              ? value.reduce(
                    (acc, element) =>
                        acc.concat([
                            null !== element
                            && 'object' === typeof element
                               ? jsonable(element)
                               : element,
                        ]), //concat()
                    []
                ) //reduce() an array
              : Object.getOwnPropertyNames(value).reduce(
                    (acc, key) =>
                        Object.assign(acc, { [key]:
                            value.hasOwnProperty(key)
                            && null !== value[key]
                            && 'object' === typeof value[key]
                                ? jsonable(value[key])
                                : value[key],
                        }), //assign()
                    {}
                ) //reduce() an object
        ); //freeze()


/*
// Nested objects.
const orig_1 = { a:1, b:2, c:{ d:1 } }
const frozen_1 = jsonable(orig_1)

console.log('Nested objects:')

if ( JSON.stringify(orig_1) !== JSON.stringify(frozen_1) )
    console.warn(`\x1b[31m✘ Is not deeply equal:\x1b[0m\n  ${JSON.stringify(orig_1)}\n  ${JSON.stringify(frozen_1)}`)
else
    console.log('\x1b[32m✓ Is deeply equal\x1b[0m')

if (orig_1 === frozen_1)
    console.warn('\x1b[31m✘ Is the original!\x1b[0m')
else
    console.log('\x1b[32m✓ Is a copy\x1b[0m')

try {
    frozen_1.a = 22
    console.error('\x1b[31m✘ Can edit top-level properties!\x1b[0m')
} catch (e) {
    console.log('\x1b[32m✓ Cannot edit top-level properties\x1b[0m')
}

try {
    frozen_1.c.d = 22
    console.error('\x1b[31m✘ Can edit 2nd-level properties!\x1b[0m')
} catch (e) {
    console.log('\x1b[32m✓ Cannot edit 2nd-level properties\x1b[0m')
}


// Nested arrays.
const orig_2 = [ 1, 2, [ 3, [ 4, 5 ], 6 ] ]
const frozen_2 = jsonable(orig_2)

console.log('Nested arrays:')

if ( JSON.stringify(orig_2) !== JSON.stringify(frozen_2) )
    console.warn(`\x1b[31m✘ Is not deeply equal:\x1b[0m\n  ${JSON.stringify(orig_2)}\n  ${JSON.stringify(frozen_2)}`)
else
    console.log('\x1b[32m✓ Is deeply equal\x1b[0m')


if (orig_2 === frozen_2)
    console.warn('\x1b[31m✘ Is the original!\x1b[0m')
else
    console.log('\x1b[32m✓ Is a copy\x1b[0m')

try {
    frozen_2[1] = 22
    console.error('\x1b[31m✘ Can edit top-level properties!\x1b[0m')
} catch (e) {
    console.log('\x1b[32m✓ Cannot edit top-level properties\x1b[0m')
}

try {
    frozen_2[2][1].push(22)
    console.error('\x1b[31m✘ Can edit 2nd-level properties!\x1b[0m')
} catch (e) {
    console.log('\x1b[32m✓ Cannot edit 2nd-level properties\x1b[0m')
}


// Mixture of objects and arrays.
const orig_3 = { a:1, b:[ 2, { c:3 } ], d:4 }
const frozen_3 = jsonable(orig_3)

console.log('Mixture of objects and arrays:')

if ( JSON.stringify(orig_3) !== JSON.stringify(frozen_3) )
    console.warn(`\x1b[31m✘ Is not deeply equal:\x1b[0m\n  ${JSON.stringify(orig_3)}\n  ${JSON.stringify(frozen_3)}`)
else
    console.log('\x1b[32m✓ Is deeply equal\x1b[0m')


if (orig_3 === frozen_3)
    console.warn('\x1b[31m✘ Is the original!\x1b[0m')
else
    console.log('\x1b[32m✓ Is a copy\x1b[0m')

try {
    frozen_3.b = 22
    console.error('\x1b[31m✘ Can edit top-level properties!\x1b[0m')
} catch (e) {
    console.log('\x1b[32m✓ Cannot edit top-level properties\x1b[0m')
}

try {
    delete frozen_3.b[1].c
    console.error('\x1b[31m✘ Can edit 2nd-level properties!\x1b[0m')
} catch (e) {
    console.log('\x1b[32m✓ Cannot edit 2nd-level properties\x1b[0m')
}
*/

/**
 * Tpe instances are immutable containers for immutable data.
 * They contain a type ‘t’, and either a payload ‘p’ or an error ‘e’.
 *
 * You can avoid throwing exceptions by passing Tpe instances around. See
 * blog.logrocket.com/76c7ae4924a1 for a rundown of ‘Left/Right’ and ‘Either’.
 *
 * @example
 *     import Tpe from './u3kit/src/tpe/-tpe.js'
 *     const numberThree = new Tpe({ p:3 })
 *     numberThree.p + 1 // 4
 *     numberThree + 1 // 4 - invokes valueOf()
 *     numberThree + "1" // "31" - invokes toString()
 *
 * @example
 *     import { Tpe } from './u3kit/bundles/u3kit.esm.js'
 *     const someError = Tpe({ e:'Oh no!' })
 *     @TODO show usage
 *
 * @public
 * @module u3kit/tpe/Tpe
 */

class Tpe {

    /**
     * Create a Tpe instance in either ‘error’ or ‘payload’ mode.
     * @param {string|object=} setup.e - An error
     * @param {*=} setup.p - Any kind of payload
     */
    constructor (...args) {
        if (1 !== args.length)
            this.e = `Tpe created with ${args.length} args`;
        else if ('object' !== typeof args[0])
            this.e = `Tpe created with '${typeof args[0]}' not 'object'`;
        else {
            const keys = Object.keys(args[0]);
            if (1 !== keys.length)
                this.e = `Tpe created with ${keys.length} keys`;
            else if ('p' === keys[0]) // payload-mode
                this.p = jsonable( args[0].p ); //@TODO check for non-jsonable p
            else if ('e' === keys[0])
                if (args[0] instanceof Error) {
                    this.e = args[0].e.message; // error-object mode
                    this._stack = args[0].e.stack; // used by diagnostic() @TODO
                } else {
                    this.e = args[0].e; // error-string mode
                }
            else
                this.e = `Tpe created with with key '`
                    + keys[0].slice(0,16)
                    + (16 < keys[0].length ? `...'` : `'`);
        }
        Object.freeze(this); // immutable @TODO deep freeze if p is object?
    }

    get t () {
        return this.e
            ? Error
            : null != this.p
                ? this.p.constructor
                : null === this.p
                    ? null
                    : undefined
    }
/*
    constructor({ e, p }) {
        if (e && null != p)
            this.e = 'Constructed with an e AND a p'
        else if (e)
            this.e = 'string' === typeof e ? e : `e is ${typeof e} not string`
        else
            this.p = p
        this.t = Number
        Object.freeze(this) // immutable @TODO deep freeze if p is object?
    }
*/
    /**
     * @TODO jsdoc
     */
    valueOf () { return null == this.p ? this.p : this.p.valueOf() }

    /**
     * @TODO jsdoc
     * @param {Number=} base - Convert a number to a string, eg `16` for hex
     */
    toString (base) { return null == this.p ? this.p+'' : this.p.toString(base) }
}

Object.freeze(Tpe);
Object.freeze(Tpe.prototype); //@TODO needed?

/**
 * Instantiates a Tpe in ‘payload’ mode.
 *
 * `* -> Tpe`
 * @param {*} p - @TODO
 * @return {Tpe} @TODO
 *
 * @example
 *     import p from './u3kit/src/tpe/p.js'
 *     p(3) + 1 // 4
 *
 * @example
 *     import { p } from './u3kit/bundles/u3kit.esm.js'
 *     ! p(false).p // true
 *     ! p(false) // true
 *
 * @public
 * @module u3kit/tpe/p
 */

const p = payload => {
    //@TODO validate args
    if (payload instanceof Tpe)
        return payload
    return new Tpe({ p:payload })
};

export { Tpe, p };
