import jsonable from './jsonable.js'

/**
 * Xx.
 *
 * `push :: (...a, String, Object) -> Object`
 *
 * @param {...*} element - One or more values to push onto the end of the array
 * @param {String=} path - Specifies where the array is. Use "" for top level
 * @param {Object} subject - A plain object or array
 * @return {Object} - an immutable copy of the object or array
 *
 * @example
 *     import push from './u3kit/src/ibeji/push.js'
 *     @TODO
 *
 * @example
 *     import { push } from './u3kit/bundles/u3kit.esm.js'
 *     @TODO
 *
 * @public
 * @module u3kit/ibeji/push
 */

const push = (element, path, subject) => {
    if (! path)
        return jsonable( subject.concat([ element ]) )
    return { a:{ b:[ { oh:'ok' }, [1,3,5,7,9] ] } }
}

export default push
