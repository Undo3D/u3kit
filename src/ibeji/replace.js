// import jsonable from './jsonable.js'

/**
 * Xx.
 *
 * `replace :: (...a, String, Object) -> Object`
 *
 * @param {*} value - The new value, to be insert where the old value was
 * @param {String=} path - Specifies the old value to be replaced
 * @param {Object} subject - A plain object or array
 * @return {Object} - an immutable copy of the object or array
 *
 * @example
 *     import replace from './u3kit/src/ibeji/replace.js'
 *     @TODO
 *
 * @example
 *     import { replace } from './u3kit/bundles/u3kit.esm.js'
 *     @TODO
 *
 * @public
 * @module u3kit/ibeji/replace
 */

const replace = (value, path, subject) => {
// path = 'fddf.ds[098]'
    path = path.split(/[.[\]]/).filter(p=>p) //@TODO deal with null match
    return Array.isArray(subject)
        ? _recursiveReplaceElement(value, path, subject)
        : _recursiveReplaceProperty(value, path, subject)
}

export default replace


function _recursiveReplaceElement (value, pathArray, subjectArray) {
    const index = +pathArray[0]
    const replacement = 1 === pathArray.length
        ? value
        : Array.isArray(subjectArray[index])
            ? _recursiveReplaceElement(value, pathArray.slice(1), subjectArray[index])
            : _recursiveReplaceProperty(value, pathArray.slice(1), subjectArray[index])
    const before = subjectArray.slice(0, index)
    const after = subjectArray.slice(index + 1)
    return [
        ...before,
        replacement,
        ...after,
    ]
}

function _recursiveReplaceProperty (property, pathArray, subjectObject) {
}
