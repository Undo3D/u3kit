export { default as always } from './kbit/always.js'
export { default as and } from './kbit/and.js'
export { default as isFunction } from './kbit/is-function.js'
