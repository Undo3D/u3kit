# Kbit

#### Functional one-liners which minify to a [kilobit](https://en.wikipedia.org/wiki/Kilobit) or less.

Inspired by (and in many cases lifted directly from)
[1-liners](https://github.com/1-liners/1-liners).

Kbit one-liners are leaf-nodes in the dependency tree. Each function is
completely self contained and __does not `import` anything.__

They are also synchronous.
