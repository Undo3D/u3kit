/**
 * Creates a function that always returns a given value.
 *
 * `always :: a -> (void -> a)`
 * @param {*} val - the value which will always be returned
 * @return {Function} takes no arguments, and only ever returns `val`
 *
 * @example
 *     import always from './u3kit/src/kbit/always.js'
 *     const T = always(true)
 *     T() //=> true
 *     T() //=> true
 *
 * @example
 *     import { always } from './u3kit/bundles/u3kit.esm.js'
 *     const fortyTwo = always(42)
 *     fortyTwo() //=> 42
 *     fortyTwo() //=> 42
 *
 * @public
 * @module u3kit/kbit/always
 */

export default val => () => val
