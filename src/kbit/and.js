/**
 * Same as `a && b`.
 *
 * `and :: (a, b) -> Boolean`
 * @param {*} a - @TODO
 * @param {*} b - @TODO
 * @return {Boolean} @TODO
 *
 * @example
 *     import and from './u3kit/src/kbit/and.js'
 *     and(true, true) // => true
 *
 * @example
 *     import { and } from './u3kit/bundles/u3kit.esm.js'
 *     and(false, true) // => false
 *
 * @public
 * @module u3kit/kbit/and
 */

export default (a, b) => !! a && !! b
