/**
 * Same as `'function' === typeof value`.
 *
 * `* -> Boolean`
 * @param {*} val - the value to test
 * @return {Boolean} `true` if `val` is a function, `false` otherwise
 *
 * @example
 *     import isFunction from './u3kit/src/kbit/is-function.js'
 *     isFunction(x => x) // => true
 *
 * @example
 *     import { isFunction } from './u3kit/bundles/u3kit.esm.js'
 *     isFunction(function named () {}) // => true
 *     isFunction('any other value')    // => false
 *
 * @public
 * @module u3kit/kbit/is-function
 */

export default val => 'function' === typeof val

// Flyd does it this way:
//     return !! ( val && val.constructor && val.call val obj.apply )
