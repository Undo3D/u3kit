# Ombla

#### A tiny but powerful library for working with reactive dataflows

Named after the [Ombla river](https://en.wikipedia.org/wiki/Ombla) in Croatia.

Inspired by [Flyd](https://github.com/paldepind/flyd).
