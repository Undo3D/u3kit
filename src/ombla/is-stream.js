import isFunction from '../kbit/is-function.js'

/**
 * Returns `true` if the supplied argument is a stream and `false` otherwise.
 *
 * `* -> Boolean`
 * @param {*} val - the value to test
 * @return {Boolean} `true` if `val` is an Ombla stream, `false` otherwise
 *
 * @example
 *     import isStream from './u3kit/src/ombla/is-stream.js'
 *     const s = stream(1)
 *     isStream(s) // => true
 *
 * @example
 *     import { isStream } from './u3kit/bundles/u3kit.esm.js'
 *     isStream(1) // => false
 *
 * @public
 * @module u3kit/ombla/is-stream
 */

export default val => isFunction(val) && 'boolean' === typeof val.hasVal
