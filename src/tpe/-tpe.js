import jsonable from '../ibeji/jsonable.js'

/**
 * Tpe instances are immutable containers for immutable data.
 * They contain a type ‘t’, and either a payload ‘p’ or an error ‘e’.
 *
 * You can avoid throwing exceptions by passing Tpe instances around. See
 * blog.logrocket.com/76c7ae4924a1 for a rundown of ‘Left/Right’ and ‘Either’.
 *
 * @example
 *     import Tpe from './u3kit/src/tpe/-tpe.js'
 *     const numberThree = new Tpe({ p:3 })
 *     numberThree.p + 1 // 4
 *     numberThree + 1 // 4 - invokes valueOf()
 *     numberThree + "1" // "31" - invokes toString()
 *
 * @example
 *     import { Tpe } from './u3kit/bundles/u3kit.esm.js'
 *     const someError = Tpe({ e:'Oh no!' })
 *     @TODO show usage
 *
 * @public
 * @module u3kit/tpe/Tpe
 */

export default class Tpe {

    /**
     * Create a Tpe instance in either ‘error’ or ‘payload’ mode.
     * @param {string|object=} setup.e - An error
     * @param {*=} setup.p - Any kind of payload
     */
    constructor (...args) {
        if (1 !== args.length)
            this.e = `Tpe created with ${args.length} args`
        else if ('object' !== typeof args[0])
            this.e = `Tpe created with '${typeof args[0]}' not 'object'`
        else {
            const keys = Object.keys(args[0])
            if (1 !== keys.length)
                this.e = `Tpe created with ${keys.length} keys`
            else if ('p' === keys[0]) // payload-mode
                this.p = jsonable( args[0].p ) //@TODO check for non-jsonable p
            else if ('e' === keys[0])
                if (args[0] instanceof Error) {
                    this.e = args[0].e.message // error-object mode
                    this._stack = args[0].e.stack // used by diagnostic() @TODO
                } else {
                    this.e = args[0].e // error-string mode
                }
            else
                this.e = `Tpe created with with key '`
                    + keys[0].slice(0,16)
                    + (16 < keys[0].length ? `...'` : `'`)
        }
        Object.freeze(this) // immutable @TODO deep freeze if p is object?
    }

    get t () {
        return this.e
            ? Error
            : null != this.p
                ? this.p.constructor
                : null === this.p
                    ? null
                    : undefined
    }
/*
    constructor({ e, p }) {
        if (e && null != p)
            this.e = 'Constructed with an e AND a p'
        else if (e)
            this.e = 'string' === typeof e ? e : `e is ${typeof e} not string`
        else
            this.p = p
        this.t = Number
        Object.freeze(this) // immutable @TODO deep freeze if p is object?
    }
*/
    /**
     * @TODO jsdoc
     */
    valueOf () { return null == this.p ? this.p : this.p.valueOf() }

    /**
     * @TODO jsdoc
     * @param {Number=} base - Convert a number to a string, eg `16` for hex
     */
    toString (base) { return null == this.p ? this.p+'' : this.p.toString(base) }
}

Object.freeze(Tpe)
Object.freeze(Tpe.prototype) //@TODO needed?
