# Tpe

#### Typed payload or error

Tpe, pronounced ‘Teepee’, refers to the properties which an instance can have:  
 - _t_ is the instance’s type
 - _p_ is its payload
 - _e_ a string or object which represents an error

Note that a Tpe instance has either a payload property ‘p’, or an error property
‘e’, but never both.

There are also three helper functions which return a Tpe instance.
 - _t()_ which creates a Tpe instance, ‘payload’ or ‘error’, after type-checking
 - _p()_ which creates a Tpe ‘payload’ instance, without type-checking
 - _e()_ which creates a Tpe ‘error’ instance
