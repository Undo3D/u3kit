import Tpe from './-tpe.js'

/**
 * Instantiates a Tpe in ‘payload’ mode.
 *
 * `* -> Tpe`
 * @param {*} p - @TODO
 * @return {Tpe} @TODO
 *
 * @example
 *     import p from './u3kit/src/tpe/p.js'
 *     p(3) + 1 // 4
 *
 * @example
 *     import { p } from './u3kit/bundles/u3kit.esm.js'
 *     ! p(false).p // true
 *     ! p(false) // true
 *
 * @public
 * @module u3kit/tpe/p
 */

export default payload => {
    //@TODO validate args
    if (payload instanceof Tpe)
        return payload
    return new Tpe({ p:payload })
}
