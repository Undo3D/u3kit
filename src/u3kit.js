// From ibeji.js
export { default as jsonable } from './ibeji/jsonable.js'
// export { default as isJsonable } from './ibeji/is-jsonable.js'
export { default as push } from './ibeji/push.js'
export { default as replace } from './ibeji/replace.js'

// From tpe.js
export { default as Tpe } from './tpe/-tpe.js'
export { default as p } from './tpe/p.js'

// From kbit.js
export { default as always } from './kbit/always.js'
export { default as and } from './kbit/and.js'
export { default as isFunction } from './kbit/is-function.js'

// From ombla.js
export { default as isStream } from './ombla/is-stream.js'

//@TODO automatically concat this file as part of the `$ npm run build` process
