// $ npm run generate-test-aggregators
//
// @TODO describe




// Ensure we’re running in Node, and that this script is in ‘u3kit/support/’.
if ('object' !== typeof process || 'function' !== typeof require)
    throw Error('Run in Node.js')
const containerDirs = __dirname.split('/').slice(-2).join('/')
if ('u3kit/support' !== containerDirs )
    throw Error(`In ‘${containerDirs}/’ not ‘u3kit/support/’`)

const
    // Load various Node modules, and get the root path.
    { readdirSync, writeFileSync, readFileSync, existsSync } = require('fs'),
    { join } = require('path'),
    root = join(__dirname, '..', 'tests'),

    // Get a list of the directories in tests/ suffixed ‘-tests’:
    // [ 'foo-bar-tests', ... ]
    testFiles =
        readdirSync(root)
       .filter( outerdir => /^[-a-z0-9]+-tests$/.test(outerdir) )

        // Get a list of the subdirectories in each ‘*-tests/’ directory:
        // [ { outerdir: 'foo-bar-tests',
        //     dirs: ['greet','some-class'],
        //   }, { ... } ]
       .map(addInnerdirs)

        // Get the .js files in each subdirectory:
        // [ { outerdir: 'foo-bar-tests',
        //     dirs: [
        //         { innerdir:'greet', innerID:'greet', files:[ 'qik-test.js' ] },
        //         { innerdir:'my-class', innerID:'myClass', files:[ 'errs.js' ] },
        //     ],
        //   }, { ... } ]
       .map(addFiles)

        // Each tests/foo-bar-tests subdirectory gets a tests/foo-bar-tests.js.
        // Add three properties to each object, ‘header’, ‘body’ and ‘footer’:
        // [ { outerdir: 'foo-bar-tests',
        //     header: '/**\n * Each test will be run against FooBar...',
        //     body: '// greet\nimport greet_src from ...',
        //     footer: '// pass to /tests/u3kit-tests.js',
        //     dirs: [
        //         { innerdir:'greet', innerID:'greet', files:[ 'qik-test.js' ] },
        //         { innerdir:'my-class', innerID:'myClass', files:[ 'errs.js' ] },
        //     ],
        //   }, { ... } ]
       .map(addHeader)
       .map(addBody)
       .map(addFooter)

// console.log( JSON.stringify(testFiles, null, 2) )
testFiles.forEach( ({ outerdir, header, body, footer }) => {
    const content = `${header}\n${body}\n${footer}`
    const path = join(root, outerdir+'.js')
    if ( existsSync(path) ) {
        const curr = (readFileSync(path) + '').split('\n').slice(0,-3).join('\n')
        if (curr === content)
            return console.log(path.split('/').pop(), 'has not changed')
    }
    writeFileSync(path, content +`
// Created by U3Kit ${require( join(__dirname, '..', 'package.json') ).version}`
+` support/generate-test-aggregators.js
// ${( new Date() ).toUTCString()}
`)
    console.log(path.split('/').pop(), 'updated!')
})


// Utilities.

function addInnerdirs (outerdir) {
    return {
        outerdir,
        dirs:
            readdirSync( join(root, outerdir) )
           .filter( sub => /^[-a-z0-9]+$/.test(sub) ),
    }
}

function addFiles ({ outerdir, dirs }) {
    return {
        outerdir,
        dirs: dirs.map( sub =>
            readdirSync( join(root, outerdir, sub) )
           .filter( file => /^[-a-z0-9]+.js$/.test(file) )
           .reduce(
                ({ innerdir, innerID, files }, file) => {
                    return { innerdir, innerID, files:files.concat(file) }
                }, {
                    innerdir: sub,
                    innerID: sub.replace(/-[a-z0-9]/g, m => m[1].toUpperCase() ),
                    files: [],
                }
            )
        ),
    }
}

function addHeader ({ outerdir, dirs }) {
    const outerLC = outerdir.slice(0,-6)
    const outerTC = outerLC[0].toUpperCase() + outerLC.slice(1)
    return {
        outerdir,
        dirs,
        header:
`/**
 * Each test will be run against ${outerTC} modules imported from:
 *   - the source code, src/${outerLC}/<module>.js
 *   - the module-aggregators, src/${outerLC}.js and src/u3kit.js
 *   - the build files, bundles/${outerLC}.*.js and bundles/u3kit.*.js
 *
 * This file imports ${outerTC} modules from all these places, imports the
 * appropriate tests, and then exports the whole lot as a big object.
 *
 * That object will be aggregated by /tests/u3kit-tests.js, and then passed
 * to /tests/run.js, which runs all tests against all source and build formats.
 *
 * @public
 * @module u3kit/tests/${outerdir}
 *
 */`,
    }
}


function addBody ({ outerdir, dirs, header }) {
    const outerLC = outerdir.slice(0,-6)
    const outerTC = outerLC[0].toUpperCase() + outerLC.slice(1)
    return {
        outerdir,
        dirs,
        header,
        body: 0 === dirs.length
            ? `// ${outerTC} has no tests yet`
            : dirs.map( ({ innerdir, innerID, files }) => `
// ${innerID}
import ${innerID}_src from '../src/${outerLC}/${innerdir}.js' // source
import { ${innerID} as ${innerID}_src_agg }    from '../src/${outerLC}.js' // aggregator
import { ${innerID} as ${innerID}_src_u3kit }  from '../src/u3kit.js'
import { ${innerID} as ${innerID}_esm }        from '../bundles/${outerLC}.esm.js'
import { ${innerID} as ${innerID}_esm_min }    from '../bundles/${outerLC}.esm.min.js'
import { ${innerID} as ${innerID}_u3_esm }     from '../bundles/u3kit.esm.js'
import { ${innerID} as ${innerID}_u3_esm_min } from '../bundles/u3kit.esm.min.js'
const ${innerID}_modules = [
    [ ${innerID}_src,        'src/${outerLC}/${innerdir}.js' ],
    [ ${innerID}_src_agg,    'src/${outerLC}.js' ],
    [ ${innerID}_src_u3kit,  'src/u3kit.js' ],
    [ ${innerID}_esm,        'bundles/u3kit.esm.js' ],
    [ ${innerID}_esm_min,    'bundles/u3kit.esm.min.js' ],
    [ ${innerID}_u3_esm,     'bundles/u3kit.esm.js' ],
    [ ${innerID}_u3_esm_min, 'bundles/u3kit.esm.min.js' ],
]\n`
+ files.map( (file, i) =>
    `import ${innerID}_${i} from './${outerdir}/${innerdir}/${file}'`
).join('\n')
+ `\nconst ${innerID}_tests = [\n`
+ files.map( (file, i) =>
    `    { tests:${innerID}_${i}, title:'${file}', path:'tests/${outerdir}/${innerdir}/${file}' },`
).join('\n')
+ '\n]'
        ).join('\n\n'),
    }
}


function addFooter ({ outerdir, dirs, header, body }) {
    const outerLC = outerdir.slice(0,-6)
    const outerTC = outerLC[0].toUpperCase() + outerLC.slice(1)
    return {
        outerdir,
        dirs,
        header,
        body,
        footer:
`

// pass to /tests/u3kit-tests.js
export default { title:'${outerTC}', tests:[
`
+ dirs.map( ({ innerID }) =>
    `    { as:'${innerID}', title:'${innerID}', tests:${innerID}_tests, modules:${innerID}_modules },`
).join('\n')
+'\n]}\n',

    }
}
