/**
 * Each test will be run against Ibeji modules imported from:
 *   - the source code, src/ibeji/<module>.js
 *   - the module-aggregators, src/ibeji.js and src/u3kit.js
 *   - the build files, bundles/ibeji.*.js and bundles/u3kit.*.js
 *
 * This file imports Ibeji modules from all these places, imports the
 * appropriate tests, and then exports the whole lot as a big object.
 *
 * That object will be aggregated by /tests/u3kit-tests.js, and then passed
 * to /tests/run.js, which runs all tests against all source and build formats.
 *
 * @public
 * @module u3kit/tests/ibeji-tests
 *
 */

// jsonable
import jsonable_src from '../src/ibeji/jsonable.js' // source
import { jsonable as jsonable_src_agg }    from '../src/ibeji.js' // aggregator
import { jsonable as jsonable_src_u3kit }  from '../src/u3kit.js'
import { jsonable as jsonable_esm }        from '../bundles/ibeji.esm.js'
import { jsonable as jsonable_esm_min }    from '../bundles/ibeji.esm.min.js'
import { jsonable as jsonable_u3_esm }     from '../bundles/u3kit.esm.js'
import { jsonable as jsonable_u3_esm_min } from '../bundles/u3kit.esm.min.js'
const jsonable_modules = [
    [ jsonable_src,        'src/ibeji/jsonable.js' ],
    [ jsonable_src_agg,    'src/ibeji.js' ],
    [ jsonable_src_u3kit,  'src/u3kit.js' ],
    [ jsonable_esm,        'bundles/u3kit.esm.js' ],
    [ jsonable_esm_min,    'bundles/u3kit.esm.min.js' ],
    [ jsonable_u3_esm,     'bundles/u3kit.esm.js' ],
    [ jsonable_u3_esm_min, 'bundles/u3kit.esm.min.js' ],
]
import jsonable_0 from './ibeji-tests/jsonable/correct-usage.js'
import jsonable_1 from './ibeji-tests/jsonable/is-frozen.js'
const jsonable_tests = [
    { tests:jsonable_0, title:'correct-usage.js', path:'tests/ibeji-tests/jsonable/correct-usage.js' },
    { tests:jsonable_1, title:'is-frozen.js', path:'tests/ibeji-tests/jsonable/is-frozen.js' },
]


// push
import push_src from '../src/ibeji/push.js' // source
import { push as push_src_agg }    from '../src/ibeji.js' // aggregator
import { push as push_src_u3kit }  from '../src/u3kit.js'
import { push as push_esm }        from '../bundles/ibeji.esm.js'
import { push as push_esm_min }    from '../bundles/ibeji.esm.min.js'
import { push as push_u3_esm }     from '../bundles/u3kit.esm.js'
import { push as push_u3_esm_min } from '../bundles/u3kit.esm.min.js'
const push_modules = [
    [ push_src,        'src/ibeji/push.js' ],
    [ push_src_agg,    'src/ibeji.js' ],
    [ push_src_u3kit,  'src/u3kit.js' ],
    [ push_esm,        'bundles/u3kit.esm.js' ],
    [ push_esm_min,    'bundles/u3kit.esm.min.js' ],
    [ push_u3_esm,     'bundles/u3kit.esm.js' ],
    [ push_u3_esm_min, 'bundles/u3kit.esm.min.js' ],
]
import push_0 from './ibeji-tests/push/examples.js'
const push_tests = [
    { tests:push_0, title:'examples.js', path:'tests/ibeji-tests/push/examples.js' },
]


// replace
import replace_src from '../src/ibeji/replace.js' // source
import { replace as replace_src_agg }    from '../src/ibeji.js' // aggregator
import { replace as replace_src_u3kit }  from '../src/u3kit.js'
import { replace as replace_esm }        from '../bundles/ibeji.esm.js'
import { replace as replace_esm_min }    from '../bundles/ibeji.esm.min.js'
import { replace as replace_u3_esm }     from '../bundles/u3kit.esm.js'
import { replace as replace_u3_esm_min } from '../bundles/u3kit.esm.min.js'
const replace_modules = [
    [ replace_src,        'src/ibeji/replace.js' ],
    [ replace_src_agg,    'src/ibeji.js' ],
    [ replace_src_u3kit,  'src/u3kit.js' ],
    [ replace_esm,        'bundles/u3kit.esm.js' ],
    [ replace_esm_min,    'bundles/u3kit.esm.min.js' ],
    [ replace_u3_esm,     'bundles/u3kit.esm.js' ],
    [ replace_u3_esm_min, 'bundles/u3kit.esm.min.js' ],
]
import replace_0 from './ibeji-tests/replace/correct-usage.js'
const replace_tests = [
    { tests:replace_0, title:'correct-usage.js', path:'tests/ibeji-tests/replace/correct-usage.js' },
]


// pass to /tests/u3kit-tests.js
export default { title:'Ibeji', tests:[
    { as:'jsonable', title:'jsonable', tests:jsonable_tests, modules:jsonable_modules },
    { as:'push', title:'push', tests:push_tests, modules:push_modules },
    { as:'replace', title:'replace', tests:replace_tests, modules:replace_modules },
]}

// Created by U3Kit 0.0.3 support/generate-test-aggregators.js
// Fri, 29 Mar 2019 22:40:22 GMT
