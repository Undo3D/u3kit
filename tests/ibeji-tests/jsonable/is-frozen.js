export default ({ it, throws, jsonable }) => it([

    'should create an immutable top-level array',
    throws( () => { let a = jsonable([1]); a[0] = 99 }, /read[- ]only/ ),
    throws( () => { let a = jsonable([1]); a.pop() }, /property/ ),

])
