const frz = Object.freeze
const fourEvens = frz([0,2,4,6])
/*
const deepOdds = frz({
    a: frz({
        b: frz([
            frz({ 'Push it':'real good' }),
            frz([1,3,5,7]),
        ]),
    }),
})
*/


export default ({ it, equal, throws, jsonEqual, push }) => it([

/* from src/ibeji/jsonable.js
 *
 *     const fourEvens = jsonable([0,2,4,6])
 *     fourEvens[3] //=> 6
 *     fourEvens[4] = 8 // throws an exception
 *     fourEvens.push(8) // throws an exception
 *     fourEvens[4] //=> undefined
 *
 *     const fiveEvens = push(8, fourEvens)
 *     fiveEvens[4] //=> 8
 */

    'should allow top-level push',
    equal( fourEvens[3], 6 ),
    throws( () => fourEvens[4] = 8, /property/ ),
    throws( () => fourEvens.push(8), /property/ ),
    equal( fourEvens[4], undefined ),
    equal(     ( () => push(8, '', fourEvens) )()[4], 8 ),
    jsonEqual( ( () => push(8, '', fourEvens) )(), [0,2,4,6,8] ),
    throws(      () => push(8, '', fourEvens).push(10), /property/ ),
    jsonEqual( fourEvens, [0,2,4,6] ), // was not changed
/*
    'should allow deep push',
    equal( deepOdds.a.b[1][3], 7 ),
    throws( () => deepOdds.a.b[1][4] = 9, /property/ ),
    throws( () => deepOdds.a.b[1].push(9), /property/ ),
    equal( deepOdds.a.b[1][4], undefined ),
    equal(     ( () => push(9, '.a.b[1]', deepOdds) )().a.b[1][4], 9 ),
    jsonEqual(
        ( () => push(8, fourEvens) )(),
        {'a':{'b':[{'Push it':'real good'},[1,3,5,7,9]]}}
    ),
// 43: '{"a":{"b":[{"oh":"ok"},[1,3,5,7,9]]}}' !== '{"a":{"b":[{"Push it":"real good"},[1,3,5,7,9]]}}'
    // throws(      () => push(8, fourEvens).push(10), /property/ ),
    // jsonEqual( fourEvens, [0,2,4,6] ), // was not changed
*/
])
