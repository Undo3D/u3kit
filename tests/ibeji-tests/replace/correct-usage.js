const frz = Object.freeze
const threeShallowEvens = frz([ 0, 2, 4 ])
const fiveShallowEvens = frz([ 0, 2, 4, 6, 8 ])
const deepOdds = frz([
    frz([ 1, 3, 5 ]),
    frz([ 7, 9, frz([ 11, 13, 15]) ]),
])

export default ({ it, equal, jsonEqual, replace }) => it([

    'should replace the middle number of a 1-level array',
    jsonEqual( replace(99, '[1]', threeShallowEvens), [ 0, 99, 4 ] ),
    equal(     replace(99, '[1]', threeShallowEvens)[1], 99 ),
    jsonEqual( replace(99, '[2]', fiveShallowEvens), [ 0, 2, 99, 6, 8 ] ),

    'should replace numbers in a 3-level array',
    jsonEqual( replace(99, '[0][2]', deepOdds),
        [ [ 1, 3, 99 ], [ 7, 9, [ 11, 13, 15 ] ] ] ),
    jsonEqual( replace(99, '[1][2][0]', deepOdds),
        [ [ 1, 3, 5 ], [ 7, 9, [ 99, 13, 15 ] ] ] ),

])
