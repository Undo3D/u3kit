/**
 * Each test will be run against Kbit modules imported from:
 *   - the source code, src/kbit/<module>.js
 *   - the module-aggregators, src/kbit.js and src/u3kit.js
 *   - the build files, bundles/kbit.*.js and bundles/u3kit.*.js
 *
 * This file imports Kbit modules from all these places, imports the
 * appropriate tests, and then exports the whole lot as a big object.
 *
 * That object will be aggregated by /tests/u3kit-tests.js, and then passed
 * to /tests/run.js, which runs all tests against all source and build formats.
 *
 * @public
 * @module u3kit/tests/kbit-tests
 *
 */

// always
import always_src from '../src/kbit/always.js' // source
import { always as always_src_agg }    from '../src/kbit.js' // aggregator
import { always as always_src_u3kit }  from '../src/u3kit.js'
import { always as always_esm }        from '../bundles/kbit.esm.js'
import { always as always_esm_min }    from '../bundles/kbit.esm.min.js'
import { always as always_u3_esm }     from '../bundles/u3kit.esm.js'
import { always as always_u3_esm_min } from '../bundles/u3kit.esm.min.js'
const always_modules = [
    [ always_src,        'src/kbit/always.js' ],
    [ always_src_agg,    'src/kbit.js' ],
    [ always_src_u3kit,  'src/u3kit.js' ],
    [ always_esm,        'bundles/u3kit.esm.js' ],
    [ always_esm_min,    'bundles/u3kit.esm.min.js' ],
    [ always_u3_esm,     'bundles/u3kit.esm.js' ],
    [ always_u3_esm_min, 'bundles/u3kit.esm.min.js' ],
]
import always_0 from './kbit-tests/always/always-correct-usage.js'
const always_tests = [
    { tests:always_0, title:'always-correct-usage.js', path:'tests/kbit-tests/always/always-correct-usage.js' },
]


// and
import and_src from '../src/kbit/and.js' // source
import { and as and_src_agg }    from '../src/kbit.js' // aggregator
import { and as and_src_u3kit }  from '../src/u3kit.js'
import { and as and_esm }        from '../bundles/kbit.esm.js'
import { and as and_esm_min }    from '../bundles/kbit.esm.min.js'
import { and as and_u3_esm }     from '../bundles/u3kit.esm.js'
import { and as and_u3_esm_min } from '../bundles/u3kit.esm.min.js'
const and_modules = [
    [ and_src,        'src/kbit/and.js' ],
    [ and_src_agg,    'src/kbit.js' ],
    [ and_src_u3kit,  'src/u3kit.js' ],
    [ and_esm,        'bundles/u3kit.esm.js' ],
    [ and_esm_min,    'bundles/u3kit.esm.min.js' ],
    [ and_u3_esm,     'bundles/u3kit.esm.js' ],
    [ and_u3_esm_min, 'bundles/u3kit.esm.min.js' ],
]
import and_0 from './kbit-tests/and/and-correct-usage.js'
import and_1 from './kbit-tests/and/and-incorrect.js'
const and_tests = [
    { tests:and_0, title:'and-correct-usage.js', path:'tests/kbit-tests/and/and-correct-usage.js' },
    { tests:and_1, title:'and-incorrect.js', path:'tests/kbit-tests/and/and-incorrect.js' },
]


// isFunction
import isFunction_src from '../src/kbit/is-function.js' // source
import { isFunction as isFunction_src_agg }    from '../src/kbit.js' // aggregator
import { isFunction as isFunction_src_u3kit }  from '../src/u3kit.js'
import { isFunction as isFunction_esm }        from '../bundles/kbit.esm.js'
import { isFunction as isFunction_esm_min }    from '../bundles/kbit.esm.min.js'
import { isFunction as isFunction_u3_esm }     from '../bundles/u3kit.esm.js'
import { isFunction as isFunction_u3_esm_min } from '../bundles/u3kit.esm.min.js'
const isFunction_modules = [
    [ isFunction_src,        'src/kbit/is-function.js' ],
    [ isFunction_src_agg,    'src/kbit.js' ],
    [ isFunction_src_u3kit,  'src/u3kit.js' ],
    [ isFunction_esm,        'bundles/u3kit.esm.js' ],
    [ isFunction_esm_min,    'bundles/u3kit.esm.min.js' ],
    [ isFunction_u3_esm,     'bundles/u3kit.esm.js' ],
    [ isFunction_u3_esm_min, 'bundles/u3kit.esm.min.js' ],
]
import isFunction_0 from './kbit-tests/is-function/is-function-test.js'
const isFunction_tests = [
    { tests:isFunction_0, title:'is-function-test.js', path:'tests/kbit-tests/is-function/is-function-test.js' },
]


// pass to /tests/u3kit-tests.js
export default { title:'Kbit', tests:[
    { as:'always', title:'always', tests:always_tests, modules:always_modules },
    { as:'and', title:'and', tests:and_tests, modules:and_modules },
    { as:'isFunction', title:'isFunction', tests:isFunction_tests, modules:isFunction_modules },
]}

// Created by U3Kit 0.0.3 support/generate-test-aggregators.js
// Fri, 22 Mar 2019 22:35:12 GMT
