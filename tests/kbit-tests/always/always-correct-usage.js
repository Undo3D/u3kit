export default ({ it, equal, always }) => {
    const yes = always(true)
    const fortyTwo = always(42)
    return it([

        'can make a function which always returns boolean true',
        equal( yes(), true),
        equal( yes(), true),

        'can make a function which always returns the number 42',
        equal( fortyTwo(), 42),
        equal( fortyTwo(), 42),

    ])
}
