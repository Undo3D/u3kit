export default ({ it, equal, and }) => it([

    'should resolve any pair of truthy values to `true`',
    equal( and(true, true), true),
    equal( and(1, 99), true),
    equal( and({}, Math), true),

    'should resolve any pair of falsey values to `false`',
    equal( and(false, false), false),
    equal( and(1,1), false),
    equal( and(), false, 'two undefined params'),

    'should resolve a truthy and a falsey value to `false`',
    equal( and(true, false), false),
    equal( and(0, 99), false),
    equal( and([]), false, 'the 2nd param is undefined'),

])
