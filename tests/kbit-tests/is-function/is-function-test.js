export default ({ it, equal, isFunction }) => it([

    'should recognise basic JavaScript functions',
    equal( isFunction(x => x), true ),
    equal( isFunction(function named () {}), true ),

    'should recognise that a string is not a function',
    equal( isFunction('any other value'), false),

])
