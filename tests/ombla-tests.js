/**
 * Each test will be run against Ombla modules imported from:
 *   - the source code, src/ombla/<module>.js
 *   - the module-aggregators, src/ombla.js and src/u3kit.js
 *   - the build files, bundles/ombla.*.js and bundles/u3kit.*.js
 *
 * This file imports Ombla modules from all these places, imports the
 * appropriate tests, and then exports the whole lot as a big object.
 *
 * That object will be aggregated by /tests/u3kit-tests.js, and then passed
 * to /tests/run.js, which runs all tests against all source and build formats.
 *
 * @public
 * @module u3kit/tests/ombla-tests
 *
 */
// Ombla has no tests yet


// pass to /tests/u3kit-tests.js
export default { title:'Ombla', tests:[

]}

// Created by U3Kit 0.0.3 support/generate-test-aggregators.js
// Fri, 22 Mar 2019 21:33:00 GMT
