import tests from './u3kit-tests.js'

// Simplified local rewrite of Tpe.
class Pe {
    constructor({ p, e }) {
        if (e && null != p)
            this.e = 'Constructed with a p AND a v'
        else if (e)
            this.e = e
        else
            this.p = p
        Object.freeze(this) // immutable @TODO deep freeze if p is object?
    }
    valueOf () { return this.p.valueOf() }
    toString (b) { return this.p.toString(b) } // `b` is for number -> string
}
const e = errorMessage => new Pe({ e:errorMessage })
const p = payload => new Pe({ p:payload })

const it = ([ ...items ]) => items.reduce(
    (passFail, item) => passFail.e
        ? passFail // already failed, earlier in the pipeline
        : 'string' === typeof item
            ? p(item) // change the title
            : item.p
                ? passFail // passed
                : e({      // failed
                      title: `It ${passFail.p}`,
                      message: item.e.message,
                      line: parseInt(item.e.line), // convert to number
                  })
  , p('start')
)

// Creates a functional `equal()` assertion-tester, tailored for a given path.
const equalFactory = path =>
    (actual, expected, message) =>
        actual === expected
            ? p('pass')
            : e({
                  message: message || `${pretty(actual)} !== ${pretty(expected)}`,
                  line: new Error().stack
                      .split('\n')
                      .filter( item => /.js:\d+:\d+/.test(item) )
                      .find( item => item.includes(path) )
                      .match(/.js:(\d+):\d+/)[1], //@TODO test on a wide range of UAs
              })

// Creates a functional `jsonEqual()` assertion-tester, tailored for a given path.
const jsonEqualFactory = path =>
    (actual, expected, message) =>
        JSON.stringify(actual) === JSON.stringify(expected)
            ? p('pass')
            : e({
                  message: message || `${pretty(JSON.stringify(actual))} !== ${pretty(JSON.stringify(expected))}`,
                  line: new Error().stack
                      .split('\n')
                      .filter( item => /.js:\d+:\d+/.test(item) )
                      .find( item => item.includes(path) )
                      .match(/.js:(\d+):\d+/)[1], //@TODO test on a wide range of UAs
              })

// Creates a `throws()` assertion-tester, tailored for a given path.
const throwsFactory = path =>
    (fn, expectedRx, message) => {
        try {
            fn()
            return e({
                message: message || `Expected ${pretty(expectedRx)} did not throw`,
                line: new Error().stack
                    .split('\n')
                    .filter( item => /.js:\d+:\d+/.test(item) )
                    .find( item => item.includes(path) )
                    .match(/.js:(\d+):\d+/)[1], //@TODO test on a wide range of UAs
            })
        } catch (actual) {
            return expectedRx.test(actual.message)
                ? p('pass')
                : e({
                      message: message || `${pretty(actual.message)} fails ${pretty(expectedRx)}`,
                      line: new Error().stack
                          .split('\n')
                          .filter( item => /.js:\d+:\d+/.test(item) )
                          .find( item => item.includes(path) )
                          .match(/.js:(\d+):\d+/)[1], //@TODO test on a wide range of UAs
                  })
        }
    }

function pretty (v) {
    const
        green  = '\x1b[32m',
        yellow = '\x1b[33m',
        cyan   = '\x1b[36m',
        reset  = '\x1b[0m',
        type = typeof v
    if (null      === v) return `${cyan}null${reset}`
    if (undefined === v) return `${cyan}undefined${reset}`
    if (''        === v) return `${green}""${reset}`
    if ('number'   === type)           return `${yellow}${v}${reset}`
    if ('function' === type && v.name) return `${cyan}${v.name}${reset}`
    if ( Array.isArray(v) )            return `${cyan}[${v.join()}]${reset}`
    if ('object' === type)             return `${cyan}${v}${reset}`
    if ('string' === type) {
        const q = ! v.includes('"') ? '"' : ! v.includes("'") ? "'" : '`'
        return `${green}${q}${v}${q}${reset}`
    }
    return v
}
//@TODO add this to `equal`:
// if (actual === expected) return this // pass
// if ( actual.valueOf() === expected.valueOf() ) return this // pass a pair of Pe instances
// if ( actual.e && ! expected.e ) //@TODO better Pe detection
//     return new Fail( message || actual.e )
// if (typeof actual !== typeof expected)
//     return new Fail(message || `${typeof actual} !== ${typeof expected}`)
// return new Fail(message || `${actual} !== ${expected}`) // fail



if ('object' === typeof document)
    document.querySelector('#test-results').innerHTML =
        format( run(tests) )
            .replace( /\x1b\[31m/g, '<span style="color:#f99">' ) // pass or string
            .replace( /\x1b\[32m/g, '<span style="color:#9f9">' ) // fail
            .replace( /\x1b\[33m/g, '<span style="color:#ff6">' ) // numbers
            .replace( /\x1b\[36m/g, '<span style="color:#6ff">' ) // functions, classes, null
            .replace( /\x1b\[0m/g, '</span>' )
else
    console.log(
        format( run(tests) )
    )

// console.log( JSON.stringify( run(tests), null, 2 ) )



// The argument can be one of three levels of test:
//   - Branch:  { title:'Some intermediate grouping', tests:[ ... ] }
//   - Twig:    { title:'Lowest-level group', modules:[ ... ], tests:[ ... ] }
//   - Leaf:    (it, equal, myModule) => { ... }
function run (vertex) {
    if ('object' !== typeof vertex) // eg a leaf-vertex (which is a function)
        throw Error(`vertex is ${typeof vertex} not object`)
    const { title, tests, modules, as } = vertex
    if (! title) throw Error(`vertex has no title`)
    if (! tests) throw Error(`vertex '${title}' has no tests`)
    return (tests && modules)
        ? runTwig(title, tests, modules, as)
        : runBranch(title, tests)
}

function runBranch (title, tests) {
    const results = tests.map(run)
    const status = results.every( r => 'pass' === r.status) ? 'pass' : 'fail'
    return { title, results, status }
}

function runTwig (title, tests, modules, as) {
    const results = tests.map( leafTest => runLeaf(leafTest, modules, as) )
    const status = results.every( r => 'pass' === r.status) ? 'pass' : 'fail'
    return { title, results, status }
}

function runLeaf ({ title, tests, path }, modules, as) {
    // console.log(title, tests, path, modules)
    const results = modules.map(
        ([ module, modulePath ]) => runLeafModule(tests, path, module, as, modulePath)
    )
    const status = results.every( r => 'pass' === r.status) ? 'pass' : 'fail'
    return { title, path, results, status }
}

function runLeafModule (tests, path, module, as, modulePath) {
    const result = tests({
        it,
        equal: equalFactory(path),
        jsonEqual: jsonEqualFactory(path),
        throws: throwsFactory(path),
        [as]: module,
    })

// console.log(modulePath, result)
    return result.p
        ? { status:'pass', modulePath }
        : { status:'fail',
            modulePath,
            title: result.e.title,
            message: result.e.message,
            line: result.e.line,
          }
    //
    // return result.e || 'pass'
    //
    // try {
    //     test(sourbun, (title, fn) => {
    //         try {
    //             fn(module)
    //         } catch (err) {
    //             throw { title:'it '+title, status:'fail', modulePath, err }
    //         }
    //     })
    // } catch (fail) {
    //     return fail
    // }
    // return { title:'ok', status:'pass', modulePath }
}


/*

// The argument can be one of three levels of test:
//   - Branch:  { title:'Huge Scope', tests:[ ... ] }
//   - Twig:    { title:'Medium Scope', modules:[ ... ], tests:[ ... ] }
//   - Leaf:    some_named_function (sourbun, it) { ... }
function run (node) {
    if ('function' === typeof node) // a leaf-node
        throw Error('oops!!')

    validateTest(node)
    const { title, modules, tests } = node

    if (tests && ! modules)
        return runBranch(title, tests)

    return runTwig(title, modules, tests)
}

function validateTest ({ title, modules, tests, test }) {
    if (! title) throw Error('No title')
    if (tests && test) throw Error(`'${title}' has \`tests\` and \`test\``)
    if (modules && test) throw Error(`'${title}' has \`modules\` and \`test\``)
}

function runBranch (title, tests) {
    const results = tests.map(run)
    const status = results.every( result => 'pass' === result.status) ? 'pass' : 'fail'
    return { title, results, status }
}

function runTwig (title, modules, tests) {
    const results = tests.map( leafTest => runLeaf(leafTest, modules) )
    const status = results.every( result => 'pass' === result.status) ? 'pass' : 'fail'
    return { title, results, status }
}

function runLeaf ({ title, test }, modules) {
    const results = modules.map(
        ([ module, modulePath ]) => runLeafWithModule(test, module, modulePath)
    )
    const status = results.every( result => 'pass' === result.status) ? 'pass' : 'fail'
    return { title, results, status }
}

function runLeafWithModule (test, module, modulePath) {
    try {
        test(sourbun, (title, fn) => {
            try {
                fn(module)
            } catch (err) {
                throw { title:'it '+title, status:'fail', modulePath, err }
            }
        })
    } catch (fail) {
        return fail
    }
    return { title:'ok', status:'pass', modulePath }
}
*/

function format ({ title, status, results, modulePath, message, line }, prev={}, depth=0) {
    // validateResult (title, status, results)
    if ('pass' === status)
        return depth ? '' : '\x1b[32mPassed all tests\x1b[0m'
    const nl = depth ? '\n' : ''
    const pad = '  '.repeat(depth)
    if (results)
        return `${nl}${pad}${title}:` + formatBranch(results, depth)
    let out = pad
    if (title !== prev.title)
        out += `${nl}${pad}${title}:`
    if (line !== prev.line || message !== prev.message)
        out += `${nl}${pad}  ${line}: ${message}`
    out += `${nl}${pad}    \x1b[31m${modulePath}\x1b[0m`
    return out
    // const out = results
    //     ? formatBranch(results, depth)
    //     : formatTwig(pad, modulePath, message, line)
    // return (depth ? '\n' : '') + `${pad}${title}:${out}`
}

function formatBranch (results, depth) {
    // console.log(1, results, depth);
    return results.map(
        (r, i) => format(r, results[i-1], depth+1)
    ).filter(r=>r).join('')
}
//
// function formatTwig (pad, modulePath, message, line) {
//     // console.log(2, pad, modulePath, message, line);
//     return `${pad}  #${line}: ${message}${pad}    ${modulePath}`
// }

// function validateResult (title, status, results, modulePath, err) {
//     if (! title)
//         throw Error('No title')
//     if ('pass' !== status && 'fail' !== status)
//         throw Error(`'${title}' has invalid status '${status}'`)
//     if (results) {
//         if (modulePath || err)
//             throw Error(`'${title}' is ambiguous`)
//     } else if (! modulePath || ! err)
//         if ('pass' !== status)
//             throw Error(`'${title}' is malformed`)
// }
