/**
 * Each test will be run against Tpe modules imported from:
 *   - the source code, src/tpe/<module>.js
 *   - the module-aggregators, src/tpe.js and src/u3kit.js
 *   - the build files, bundles/tpe.*.js and bundles/u3kit.*.js
 *
 * This file imports Tpe modules from all these places, imports the
 * appropriate tests, and then exports the whole lot as a big object.
 *
 * That object will be aggregated by /tests/u3kit-tests.js, and then passed
 * to /tests/run.js, which runs all tests against all source and build formats.
 *
 * @public
 * @module u3kit/tests/tpe-tests
 *
 */

// Tpe
import Tpe_src from '../src/tpe/-tpe.js' // source
import { Tpe as Tpe_src_agg }    from '../src/tpe.js' // aggregator
import { Tpe as Tpe_src_u3kit }  from '../src/u3kit.js'
import { Tpe as Tpe_esm }        from '../bundles/tpe.esm.js'
import { Tpe as Tpe_esm_min }    from '../bundles/tpe.esm.min.js'
import { Tpe as Tpe_u3_esm }     from '../bundles/u3kit.esm.js'
import { Tpe as Tpe_u3_esm_min } from '../bundles/u3kit.esm.min.js'
const Tpe_modules = [
    [ Tpe_src,        'src/tpe/-tpe.js' ],
    [ Tpe_src_agg,    'src/tpe.js' ],
    [ Tpe_src_u3kit,  'src/u3kit.js' ],
    [ Tpe_esm,        'bundles/u3kit.esm.js' ],
    [ Tpe_esm_min,    'bundles/u3kit.esm.min.js' ],
    [ Tpe_u3_esm,     'bundles/u3kit.esm.js' ],
    [ Tpe_u3_esm_min, 'bundles/u3kit.esm.min.js' ],
]
import Tpe_0 from './tpe-tests/-tpe/constructor-misuse.js'
import Tpe_1 from './tpe-tests/-tpe/correct-usage.js'
const Tpe_tests = [
    { tests:Tpe_0, title:'constructor-misuse.js', path:'tests/tpe-tests/-tpe/constructor-misuse.js' },
    { tests:Tpe_1, title:'correct-usage.js', path:'tests/tpe-tests/-tpe/correct-usage.js' },
]


// p
import p_src from '../src/tpe/p.js' // source
import { p as p_src_agg }    from '../src/tpe.js' // aggregator
import { p as p_src_u3kit }  from '../src/u3kit.js'
import { p as p_esm }        from '../bundles/tpe.esm.js'
import { p as p_esm_min }    from '../bundles/tpe.esm.min.js'
import { p as p_u3_esm }     from '../bundles/u3kit.esm.js'
import { p as p_u3_esm_min } from '../bundles/u3kit.esm.min.js'
const p_modules = [
    [ p_src,        'src/tpe/p.js' ],
    [ p_src_agg,    'src/tpe.js' ],
    [ p_src_u3kit,  'src/u3kit.js' ],
    [ p_esm,        'bundles/u3kit.esm.js' ],
    [ p_esm_min,    'bundles/u3kit.esm.min.js' ],
    [ p_u3_esm,     'bundles/u3kit.esm.js' ],
    [ p_u3_esm_min, 'bundles/u3kit.esm.min.js' ],
]
import p_0 from './tpe-tests/p/correct-usage.js'
import p_1 from './tpe-tests/p/is-frozen.js'
const p_tests = [
    { tests:p_0, title:'correct-usage.js', path:'tests/tpe-tests/p/correct-usage.js' },
    { tests:p_1, title:'is-frozen.js', path:'tests/tpe-tests/p/is-frozen.js' },
]


// pass to /tests/u3kit-tests.js
export default { title:'Tpe', tests:[
    { as:'Tpe', title:'Tpe', tests:Tpe_tests, modules:Tpe_modules },
    { as:'p', title:'p', tests:p_tests, modules:p_modules },
]}

// Created by U3Kit 0.0.3 support/generate-test-aggregators.js
// Sat, 23 Mar 2019 17:54:06 GMT
