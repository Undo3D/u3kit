export default ({ it, equal, Tpe }) => it([

    'should record a given payload ‘p’',
    equal( new Tpe({ p:123 }).p, 123 ),
    // equal( new Tpe({ p:123 }).t, Number ),
    equal( new Tpe({ p:undefined }).p, undefined ),
    // equal( new Tpe({ p:undefined }).t, Number ),

    'should record a given error ‘e’',
    equal( new Tpe({ e:'Here’s an error' }).e, 'Here’s an error' ),
    equal( new Tpe({ e:{ also:'an error' } }).e.also, 'an error' ),

])
