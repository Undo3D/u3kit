const arr = {
    empty: [],
    nums: [ 1, 2, 3 ],
    inst: new Array(4, 5, 6),
}
// const err = {
//     empty: new Error(),
//     range: new RangeError('Out of range'),
// }
// const fn = {
//     anon: x => x,
//     named: function someFn (x) { return x },
//     inst: new Function('x', 'return x'),
//     class: class SomeClass {},
// }
const obj = {
    empty: {},
    nums: { a:1, b:2, c:3, toString () { return 'Hi ' } },
    inst: new Object(),
}
obj.inst.valueOf = () => 1000
obj.inst.toString = () => 'Hello '
// const rx = {
//     literal: /abc/g,
//     inst: new RegExp('xyz','g'),
// }

export default ({ it, equal, jsonEqual, p }) => it([

    'should return a Tpe instance',
    equal( p().constructor.name, 'Tpe' ),

    'should allow arrays (see also p/freezes-payload.js)',
    jsonEqual( p(arr.empty).p, arr.empty ),
    jsonEqual( p(arr.nums).p, arr.nums ),
    jsonEqual( p(arr.inst).p, arr.inst ),
    jsonEqual( p(arr.empty).valueOf(), arr.empty ),
    jsonEqual( p(arr.nums).valueOf(), arr.nums ),
    jsonEqual( p(arr.inst).valueOf(), arr.inst ),
    equal( p(arr.empty) + 2, '2' ),
    equal( p(arr.nums) + 5, '1,2,35' ),
    equal( p(arr.inst) + 8, '4,5,68' ),
    equal( p(arr.empty).toString(), '' ),
    equal( p(arr.nums).toString(), '1,2,3' ),
    equal( p(arr.inst).toString(2), '4,5,6' ), // nb the `2` does nothing here
    equal( p(arr.empty).t, Array ),
    equal( p(arr.nums).t, Array ),
    equal( p(arr.inst).t, Array ),

    'should allow boolean false',
    equal( p(false).p, false ),
    equal( p(false).valueOf(), false ),
    equal( p(false).toString(), 'false' ),
    equal( p(false).p + true, 1 ),
    equal( p(false).t, Boolean ),
    equal( p( Boolean(0) ).t, Boolean ),

    'should allow boolean true',
    equal( p(true).p, true ),
    equal( p(true).valueOf(), true ),
    equal( p(true).toString(), 'true' ),
    equal( p( Boolean({}) ).p + true, 2 ),
    equal( p(true).t, Boolean ),
    equal( p( Boolean(1) ).t, Boolean ),

    'should make a ‘false’ Tpe which coerces to true',
    equal( ! p(false), false ),
    equal( p(false) && true, true ),
    equal( !! p( Boolean(0) ), true ),

    'should make a ‘true’ Tpe which also coerces to true',
    equal( ! p(true), false ),
    equal( p(true) && true, true ),
    equal( !! p( Boolean(1) ), true ),
    //
    // 'should allow Errors (treated like any other payload)',
    // jsonEqual( p(err.empty).p, err.empty ),
    // jsonEqual( p(err.range).p, err.range ),
    // jsonEqual( p(err.empty).valueOf(), err.empty ),
    // jsonEqual( p(err.range).valueOf(), err.range ),
    // equal( p(err.empty).toString(), 'Error' ),
    // equal( p(err.range).toString(), 'RangeError: Out of range' ),
    // equal( p(err.empty) + 2, 'Error2' ),
    // equal( p(err.range) + 2, 'RangeError: Out of range2' ),
    // equal( p(err.empty).t, Error ),
    // equal( p(err.range).t, RangeError ),
    //@TODO

    // 'should allow functions',
    // equal( p(fn.anon).p, fn.anon ),
    // equal( p(fn.named).p, fn.named ),
    // equal( p(fn.inst).p, fn.inst ),
    // equal( p(fn.class).p, fn.class ),
    // equal( p(fn.anon).p.name, 'anon' ),
    // equal( p(fn.named).p.name, 'someFn' ),
    // equal( p(fn.inst).p.name, 'anonymous' ),
    // equal( p(fn.class).p.name, 'SomeClass' ),
    // equal( p(fn.anon).valueOf(), fn.anon ),
    // equal( p(fn.named).valueOf(), fn.named ),
    // equal( p(fn.inst).valueOf(), fn.inst ),
    // equal( p(fn.class).valueOf(), fn.class ),
    // equal( p(fn.anon).toString(), fn.anon.toString() ),
    // equal( p(fn.named)+'', fn.named+'' ),
    // equal( p(fn.inst) + 1, fn.inst + '1' ), // is this what you’d expect?
    // equal( p(fn.class).toString(), fn.class+'' ),
    // equal( p(fn.anon).t, Function ),
    // equal( p(fn.named).t, Function ),
    // equal( p(fn.inst).t, Function ),
    // equal( p(fn.class).t, Function ),
    //@TODO

    'should allow null',
    equal( p(null).p, null ),
    equal( p(null).valueOf(), null ),
    equal( p(null).toString(), 'null' ),
    equal( p(null).p + 5, 5 ),
    equal( p(null).t, null ),

    'should allow numbers',
    equal( p(11).p, 11 ),
    equal( p(22).valueOf(), 22 ),
    equal( p(22) + 2, 24 ),
    equal( p( Number(22) ) + 2, 24 ),
    equal( p(33).toString(), '33' ),
    equal( p(33) + '2', '332' ),
    equal( p(44).toString(16), '2c' ),
    equal( p(55).t, Number ),
    equal( p( Number(55) ).t, Number ),
    equal( p(Infinity).t, Number ),
    equal( p(-0).t, Number ),
    equal( p(NaN).t, Number ),

    'should allow objects (see also p/freezes-payload.js)',
    jsonEqual( p(obj.empty).p, obj.empty ),
    jsonEqual( p(obj.nums).p, obj.nums ),
    jsonEqual( p(obj.inst).p, obj.inst ),
    jsonEqual( p(obj.empty).valueOf(), obj.empty ),
    jsonEqual( p(obj.nums).valueOf(), obj.nums ),
    jsonEqual( p(obj.inst).valueOf(), 1000 ),
    equal( p(obj.empty) + 2, '[object Object]2' ),
    equal( p(obj.nums) + 5, 'Hi 5' ),
    equal( p(obj.inst) + 8, 1008 ),
    equal( p(obj.empty).toString(), '[object Object]' ),
    equal( p(obj.nums) + '!', 'Hi !' ),
    equal( p(obj.inst) + 'Undo3D', '1000Undo3D' ), // expected 'Hello Undo3D'... @TODO investigate
    equal( p(obj.empty).t, Object ),
    equal( p(obj.nums).t, Object ),
    equal( p(obj.inst).t, Object ),

    // 'should allow regular expressions',
    // equal( p(rx.literal).p, rx.literal ),
    // equal( p(rx.inst).p, rx.inst ),
    // equal( p(rx.literal).valueOf(), rx.literal ),
    // equal( p(rx.inst).valueOf(), rx.inst ),
    // equal( p(rx.literal) + 2, '/abc/g2' ),
    // equal( p(rx.inst) + 5, '/xyz/g5' ),
    // equal( p(rx.literal).toString(), '/abc/g' ),
    // equal( p(rx.inst) + '99', '/xyz/g99' ),
    // equal( p(rx.literal).t, RegExp ),
    // equal( p(rx.inst).t, RegExp ),
    //@TODO

    'should allow strings',
    equal( p('').p, '' ),
    equal( p('a').p, 'a' ),
    equal( p('b').valueOf(), 'b' ),
    equal( p('b') + 'B', 'bB' ),
    equal( p( String(true) ) + '!', 'true!' ),
    equal( p('c').toString(), 'c' ),
    equal( parseInt( p('44') ), 44 ),
    equal( parseInt( p(55) ), 55 ),
    equal( p('66').t, String ),
    equal( p( String() ).t, String ),

    'should allow undefined',
    equal( p().p, undefined ),
    equal( p( void(0) ).p, undefined ),
    equal( p(undefined).p, undefined ),
    equal( p(undefined).valueOf(), undefined ),
    equal( p(undefined).toString(), 'undefined' ),
    equal( isNaN( p(undefined).p + 5 ), true ),
    equal( p(undefined).t, undefined ),

])
