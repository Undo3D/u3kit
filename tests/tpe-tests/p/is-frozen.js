export default ({ it, throws, p }) => it([

    'should create an immutable Tpe instance',
    throws( () => { let one = p(1); one.p = 99 }, /read[- ]only/ ),
    throws( () => { let ok = p('ok'); ok.t = String }, /getter/ ),
    throws( () => { let two = p(3); two.nope = 123 }, /extensible/ ),

    'should prevent mutation of an array payload',
    throws( () => { let arr = p([1]); arr.p[1] = 2 }, /property/ ),
    throws( () => { let arr = p([1]); arr.p[0] = 8 }, /read[- ]only/ ),
    throws( () => { let arr = p([1]); arr.p.pop() }, /property/ ),
    throws( () => { let arr = p([1]); arr.p.a = 'z' }, /extensible/ ),
    throws( () => { let arr = p([{a:'a'}]); arr.p[0].a = 'z' }, /read[- ]only/ ),

])
