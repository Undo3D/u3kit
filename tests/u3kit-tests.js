import ibeji_tests from './ibeji-tests.js'
import tpe_tests from './tpe-tests.js'
// import kbit_tests from './kbit-tests.js'
// import ombla_tests from './ombla-tests.js'
// import sourbun_tests from './sourbun-tests.js'

export default {
    title: 'U3Kit',
    tests: [
        ibeji_tests,
        tpe_tests,
        // kbit_tests,
        // ombla_tests,
        // sourbun_tests,
    ],
}
